package com.example.animationtutorial.Object_animator_resources;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.example.animationtutorial.R;

public class Main2Activity extends AppCompatActivity {

    ImageView imageView;

    Button fade;
    Button trans;
    Button rotate;
    Button scale;
    Button combo;
    Button test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        imageView = findViewById(R.id.image);
        fade = findViewById(R.id.button_alpha);
        trans = findViewById(R.id.button_trans);
        rotate = findViewById(R.id.button_rotate);
        scale = findViewById(R.id.button_scale);
        combo = findViewById(R.id.button_combo);
        test = findViewById(R.id.button_test);

        mySetup(fade, R.animator.fade);
        mySetup(trans, R.animator.translation);
        mySetup(rotate, R.animator.rotate);
        mySetup(scale, R.animator.scale);
        mySetup(combo, R.animator.combo);


        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewRotator viewRotator = new ViewRotator();
                viewRotator.startRotating(v);
//                Animation anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.bounce);
////                double animationDuration = 3000;
////                anim.setDuration((long) animationDuration);
////
////                MyBounceInterpolator interpolator =new MyBounceInterpolator(0.3, 18.5);
////                anim.setInterpolator(interpolator);
////
////
////                imageView.startAnimation(anim);
            }
        });


    }

    public void mySetup(View view, final int resource) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animator anim = AnimatorInflater.loadAnimator(Main2Activity.this, resource);
                anim.setTarget(imageView);
                anim.start();
            }
        });
    }


}
