package com.example.animationtutorial.ControlButtons;

import android.animation.ObjectAnimator;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.animationtutorial.R;

public class Main3Activity extends AppCompatActivity {
    private ImageView imageView;
    private Button start;
    private Button stop;
    private Button cancel;
    private Button pause;
    private Button resume;
    private ObjectAnimator anim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        bind();

        anim = ObjectAnimator.ofFloat(imageView, View.ROTATION, 0, 360);
        anim.setDuration(2000);
        anim.setRepeatCount(5);
        anim.setRepeatMode(ObjectAnimator.RESTART);


    }

    private void bind() {
        imageView = findViewById(R.id.image_one);
        start = findViewById(R.id.button_start);
        stop = findViewById(R.id.button_stop);
        cancel = findViewById(R.id.button_cancel);
        pause = findViewById(R.id.button_pause);
        resume = findViewById(R.id.button_resume);
    }

    public void start(View view) {
        anim.start();
    }

    public void stop(View view) {
        anim.cancel();
    }

    public void cancel(View view) {
        anim.cancel();
    }

    public void pause(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            anim.pause();
        } else {
            anim.cancel();
        }
    }

    public void resume(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            anim.resume();
        } else {
            anim.start();
        }
    }

}
