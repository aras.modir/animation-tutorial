package com.example.animationtutorial.AnimatorVaue;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.animationtutorial.R;

public class Main4Activity extends AppCompatActivity {
    LinearLayout linearLayout;
    Button button1;
    Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        linearLayout = findViewById(R.id.mainLinearLayout);
        button1 = findViewById(R.id.button_1);
        button2 = findViewById(R.id.button_2);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ValueAnimator valueAnimator = new ValueAnimator();
                valueAnimator.setIntValues(Color.parseColor("#f44336"),
                        Color.parseColor("#9c27b0"));

                valueAnimator.setEvaluator(new ArgbEvaluator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        linearLayout.setBackgroundColor((Integer) animation.getAnimatedValue());
                    }
                });

                valueAnimator.setDuration(4000);
                valueAnimator.start();
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ValueAnimator valueAnimator = new ValueAnimator();
                valueAnimator.setIntValues(Color.parseColor("#ffffff"),
                        Color.parseColor("#ff9900"));

                valueAnimator.setEvaluator(new ArgbEvaluator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        linearLayout.setBackgroundColor((Integer) animation.getAnimatedValue());
                    }
                });

                valueAnimator.setDuration(4000);
                valueAnimator.start();
            }
        });

    }
}
