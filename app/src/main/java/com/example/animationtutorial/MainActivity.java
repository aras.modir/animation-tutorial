package com.example.animationtutorial;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView imageView;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.img);
        button = findViewById(R.id.start_button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ObjectAnimator alphA = ObjectAnimator.ofFloat(imageView, View.ALPHA, 0, 1);
                alphA.setDuration(4000);

                ObjectAnimator transA = ObjectAnimator.ofFloat(imageView, View.TRANSLATION_X, -500, 0);
                transA.setDuration(4000);

                ObjectAnimator rotateA = ObjectAnimator.ofFloat(imageView, View.ROTATION, 0, 360);
                rotateA.setDuration(4000);

                PropertyValuesHolder phyx = PropertyValuesHolder.ofFloat(View.SCALE_X,0,1);
                PropertyValuesHolder phyy = PropertyValuesHolder.ofFloat(View.SCALE_Y,0,1);

                ObjectAnimator scaleA = ObjectAnimator.ofPropertyValuesHolder(imageView, phyx, phyy);
                scaleA.setDuration(4000);

                AnimatorSet anim = new AnimatorSet();
//                anim.play(alphA).before(transA).after(rotateA).with(scaleA);
//                anim.playSequentially(alphA,transA, rotateA, scaleA);
                anim.playTogether(alphA,transA, rotateA, scaleA);

                anim.start();
            }
        });


    }
}
